<div id="about">
<div id ="poll_view" ng-app ng-controller="polls_view">

    <div id ="slider">
        <label for=fader>Money?</label>
        <input type=range min=0 max=40 value="{{money}}" id=fader step=1 ng-model="money">
        <output for=fader id=money>{{money}}</output> 
                <br>
        <button ng-click="getFood(money)" id = "moneysubmit" type="button">Lego</button>
    </div>
    <br>
    <br>
    
    <div id = "PollingBoothAnswersOuter">
        <label>This is what you can afford</label>
             <div id = "PollingBoothAnswers">
                <table class="table table-striped table-condensed table-bordered">
                  <tr ng-repeat="answer in answers">
                    <td ng-click="setAnswer(answer.id,answer.poll)"> {{answer.text}}<br></td>
                  </tr>
                </table>
        <br>
        <button ng-click="submitVote()" id = "questionsubmit" type="button">Submit</button>
    </div>
   </div>
</div>

<script>
function polls_view($scope, $http) {
    
   
  // Initialising the variable.
  $scope.answers = [];
  $scope.answer = ' ';
  $scope.answerSelected = false;
  $scope.money = 0;
  data = null;
  
  // When you click on a poll the question box is displayed
  $scope.getAffordableItems = function() {
        var money = $scope.money;
        data = {money:money}
        JSON.stringify(data);
        $http({
        url: 'services/json_get_answers',
        method: "POST",
        datatype : "json",
        data: $.param(data),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
        $scope.answers = data;
        });
        
        }
      
  //Slider value shower 
  $scope.getFood = function(money){
    alert(money);
    $scope.getAffordableItems();
  }



}

</script>
</div>