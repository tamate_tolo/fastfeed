<?php
class Poll extends CI_Model {
  function __construct() {
        parent::__construct();
        $this->load->database();
  }
  function get_json_polls_data() {
        $this->db->select();
        $this->db->from('Polls');
        $this->db->order_by('id');
        $query = $this->db->get();
    return $query->result();
  }
    function get_json_answers_data($id) {
        $this->db->from('Answers');
        $this->db->where("answerSet",$id);
        $this->db->order_by('id');
        $query = $this->db->get();
    return $query->result();
  }
   function get_json_votes_data($id) {
        $this->db->from("Votes");
        $this->db->join("Answers", "Votes.poll_answer_selected = Answers.id",'right');
        $this->db->select('Answers.text, Votes.poll_answer_selected, COUNT(Votes.poll_answer_selected) as total');
        $this->db->group_by('Answers.text');
        $this->db->where("Answers.poll",$id);
        $this->db->order_by('total', 'desc'); 
        $query = $this->db->get();
    return $query->result();
  }
    function post_json_vote_data($ip,$answer,$poll) {
        $this->load->database();
        $this->db->from('Votes');
        $data = array(
            'user'=>$ip,
            'poll'=>$poll,
            'poll_answer_selected'=>$answer,
            );
        $this->db->insert('Votes',$data);
        return "All is well";
  }
  
}