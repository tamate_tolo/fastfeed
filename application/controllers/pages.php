<?php

class Pages extends CI_Controller {

	public function view($page = 'home')
	{
            
            if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
            {
                    // Whoops, we don't have a page for that!
                    show_404();
            }

            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->helper('html');
            $data['title'] = ucfirst($page);  // Capitalize the first letter
            $data['content'] = $this->load->view('pages/'.$page, $data, TRUE); 
            $this->load->view('templates/master', $data);


	}
        public function get_news($slug = FALSE)
{
	if ($slug === FALSE)
	{
		$query = $this->db->get('news');
		return $query->result_array();
	}

	$query = $this->db->get_where('news', array('slug' => $slug));
	return $query->row_array();
}
}

