// A version of jsdemo.js written using jQuery.
// Written for SENG365 by Richard Lobb.
// This version June 2014.


// The jsdemo script rewritten using jquery.

(function () {
    "use strict";

    /*jslint browser: true, devel: true, indent: 4, maxlen: 80 */


    function sayHi(e) {
        alert('Hi');
    }

    // Display the feel-nice span, then fade out over 2 seconds
    function coo(e) {
        $("#feel-nice-span").show().fadeOut(2000);
    }


    // Sets the current element to a red 'Thank you!'
    function changeInvitationSpan(e) {
        $(this).html('Thank you!').css("color", "red");
    }


    // Add an item to the item list
    function addItem(e) {
        var numItems = $("#item-list li").size(),
            newItem = "<li>Item " + (numItems + 1) + "</li>";
        $("#item-list").append(newItem);
    }


    // Delete an item from the item list
    function deleteItem(e) {
        $("#item-list li:last").remove();
    }

    // Display only those rows of 'bods-table' containing the string
    // specified by the "filter-input" field.
    function filterTableItems(e) {
        var stringToMatch = $('#filter-input').val();
        stringToMatch.replace("'", "\\'"); // Escape single quotes
        $("#bods-table tr").show();
        if (stringToMatch !== '') {
            $("#bods-table tr:gt(0):not(:contains('" + stringToMatch +
                "'))").hide();
        }
    }
    
    // Function called when reset button is clicked
        $("#reset-button").click(function(){
            $("#filter-input").val("");
            $("#invitation-span").html("Double click me please");
            $("#invitation-span").css('color', 'black')
            });

    // An alternative version of filterTableItems. Which do you prefer?
    function filterTableItems2(e) {
        var stringToMatch = $('#filter-input').val();
        $("#bods-table tr:gt(0)").each(function () {
            if ($(this).html().indexOf(stringToMatch) >= 0) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    }

    // Main body:registers the event handlers on the various elements

    $("#touch-me-span").mouseover(coo);
    $("#click-me-span").click(sayHi);
    $("#add-item-button").click(addItem);
    $("#invitation-span").dblclick(changeInvitationSpan);
    $("#delete-item-button").click(deleteItem);
    $("#filter-input").keyup(filterTableItems2);

// End of the anonymous function
}());

