<!DOCTYPE html>

<html>
    <head>
        <link href="<?php echo base_url("styles/style.css"); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url("styles/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css">
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body ng-app ng-controller="master">
        <h1 id = 'banner'> Fast Feed </h1>
        <!------
         <ul id = 'navbar'>

            <li><a href="<?php echo site_url('poll-browser'); ?>">Vote Now</a></li>
            <li><a href="<?php echo site_url('poll-results'); ?>">View Poll Results</a></li>
            <li><a href="<?php echo site_url('about'); ?>">About</a></li>
            <li><a id ="dangerbutton" ng-click = "deletevotes()" href="#">Clear Votes</a></li>
        </ul>
        ---->
        <hr>
        <?php echo $content; ?>
        <hr>
        
        <script src="<?php echo base_url('scripts/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('scripts/angular.min.js'); ?>"></script>
        <script src="<?php echo base_url('scripts/bootstrap.min.js'); ?>"></script>
        
        <footer>
            <p style="font-weight:bold">&copy; 2013</p>
        </footer>
        
        <script>
            function master($scope, $http) {
                  $scope.deletevotes = function () {
                  $http({
                        url: 'services/delete_votes',
                        method: "DELETE",
                        }).success(function () {
                          alert("All Votes Deleted");
                        });
                  }
            }
        </script>


    </body>

</html>