<div id="about">
<h3> About </h3>

<p>
    Poll Position is a web application where we ask you questions and then sell your answers to companies so they can email you annoying spam. 
</p>

<h3> Features </h3>
<ul>
    <li> Access all Polling Booths without refreshing entire Page</li>
    <li> View results for any Polling booth easily</li>    
    <li> Delete all votes </li>  
</ul>

<h3> How to use </h3>
<p> Select from a polling booth on the left pane. It's questions will appear on the right and you will be able to select an answer and hit submit.
    <br>
    Deleting all votes can be achieved by clicking on clear votes.
</p>

<h3> Known Bugs </h3>
<p> 
     None so far :)
</p>

<p>
    @Author: Tamate Tolo
    <br>
    @StudentID: 45823249
</p>
</div>