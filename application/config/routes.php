<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = 'pages/view';
$route['poll-browser'] = 'services/vote';
$route['poll-results'] = 'services/results';
$route['about'] = 'pages/view';