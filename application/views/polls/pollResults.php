<div id ="poll_results" ng-app ng-controller="polls_results">
   
  
    <div id =PollingRBoothOuter>
        <label>Select a booth</label>
            <div id = "PollingRBooth">
                <table class="table table-condensed table-bordered">
                  <tr ng-repeat="poll in polls">
                    <td ng-click="setSelected(poll.id,poll.question,poll.title)" ng-class="{selected: poll.id === idSelectedPoll}">{{poll.title}}</td>
                  </tr>
                </table>
            </div>
       </div>
    <div id = "PollingResultsOuter">
        <label>{{question}}</label>
             <div id = "PollingRBooth">
                <table class="table table-striped table-condensed table-bordered">
                        <tr>
                        <th>{{title}}</th>
                        <th>Total Votes</th>
                       </tr>
                  <tr ng-repeat="vote in votes">
                      <td>{{vote.text}}</td>
                      <td>{{vote.total}}</td>
                  </tr>
                </table>
        <br>
        
    </div>
   </div>
</div>

<script>
function polls_results($scope, $http) {
    
   
  // Initialising the variable.
  
  $scope.youSureBruh = false;
  $scope.polls = [];
  $scope.votes = [];
  $scope.answers = [];
  $scope.idSelectedPoll = null;
  $scope.question = ' ';
  $scope.title = '';
  data = null;

  // Getting the list of polls through ajax call.
  $http({
    url: 'services/polls',
    method: "GET",
    }).success(function (data) {
    $scope.polls = data;
  });
  
  // Highlight selected poll and then show its questions
  $scope.setSelected = function (idSelectedPoll,question,title) {
            $scope.idSelectedPoll = idSelectedPoll;
            $scope.question = question;
            $scope.title = title;
            $scope.addanswers();
            $scope.getVotes();
            //Make Votes visible
            $("#PollingResultsOuter").css('visibility', 'visible');
  };
  
  $scope.addanswers = function() {
        var id = $scope.idSelectedPoll;
        data = {id:id};
        JSON.stringify(data);
        $http({
        url: 'services/json_get_answers',
        method: "POST",
        datatype : "json",
        data: $.param(data),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            $scope.answers = data;
        });
        
  }
  
  // When you click on a poll the question box is displayed
  $scope.getVotes = function() {
        var id = $scope.idSelectedPoll;
        data = {id:id};
        JSON.stringify(data);
        $http({
        url: 'services/json_get_votes',
        method: "POST",
        datatype : "json",
        data: $.param(data),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            $scope.votes = data;
        });
        }      

}
</script>