<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {
  function __construct() {
    parent::__construct();
  }
  function delete_votes(){
    $this->db->truncate('Votes'); 
  }
  
  function vote() {
    $data['title'] = 'Poll Viewer';
    $data['viewname'] = 'pollBrowser';
    $data['content'] = $this->load->view('polls/pollBrowser',$data, TRUE);
    $this->load->helper('html');
    $this->load->view('templates/master',$data);
  }
  
  function results() {
    $data['title'] = 'Poll Results';
    $data['viewname'] = 'pollResults';
    $data['content'] = $this->load->view('polls/pollResults',$data, TRUE);
    $this->load->helper('html');
    $this->load->view('templates/master',$data);
  }
   
  function polls() {    
    $this->load->model('Poll','poll');
    $data = $this->poll->get_json_polls_data();
    print json_encode($data);
  }
  
 function json_get_answers()
  //Reuseing the model, no point making a new one for answers
  { 
    $id = $this->input->post('money');
    $this->load->model('Poll','poll');
    $data = $this->poll->get_json_answers_data($id);
    print json_encode($data);
  }
   function json_get_votes()
  { 
    $id = $this->input->post('id');
    $this->load->model('Poll','poll');
    $data = $this->poll->get_json_votes_data($id);
    print json_encode($data);
  }
  
  function submit_vote()
  {
    // Get IP address
    $ip = gethostbyname(trim(`hostname`));
    $answer = $this->input->post('answer');
    $poll = $this->input->post('poll');
    $this->load->model('Poll','poll');
    $data = $this->poll->post_json_vote_data($ip,$answer,$poll);
    print json_encode($data);
  }
}
