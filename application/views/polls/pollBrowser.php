<div id ="poll_view" ng-app ng-controller="polls_view">
   
  
    <div id =PollingBoothOuter>
        <label>Select a booth</label>
            <div id = "PollingBooth">
                <table class="table table-condensed table-bordered">
                  <tr ng-repeat="poll in polls">
                    <td ng-click="setSelected(poll.id,poll.question)" ng-class="{selected: poll.id === idSelectedPoll}">{{poll.title}}</td>
                  </tr>
                </table>
            </div>
       </div>
    <h1 id = "success"> Thanks for voting! </h1>
    
    <div id = "PollingBoothAnswersOuter">
            
        <label>{{question}}</label>
             <div id = "PollingBoothAnswers">
                <table class="table table-striped table-condensed table-bordered">
                  <tr ng-repeat="answer in answers">
                    <td ng-click="setAnswer(answer.id,answer.poll)"> <input type="radio" name="questions" value="male">{{answer.text}}<br></td>
                  </tr>
                </table>
        <br>
        <button ng-click="submitVote()" id = "questionsubmit" type="button">Submit</button>
    </div>
   </div>
</div>

<script>
function polls_view($scope, $http) {
    
   
  // Initialising the variable.
  $scope.polls = [];
  $scope.answers = [];
  $scope.idSelectedPoll = null;
  $scope.question = ' ';
  $scope.answer = ' ';
  $scope.poll = ' ';
  $scope.answerSelected = false;
  data = null;

  // Getting the list of polls through ajax call.
  $http({
    url: 'services/polls',
    method: "POST",
  }).success(function (data) {
    $scope.polls = data;
  });
  
  // Highlight selected poll and then show its questions
  $scope.setSelected = function (idSelectedPoll,question) {
            $scope.idSelectedPoll = idSelectedPoll;
            $scope.question = question;
            //Turn off answerSelected in the case that the user switches poll
            $scope.answerSelected = false;
            $scope.addanswers();
            //Make submit visible
            $("#PollingBoothAnswersOuter").css('visibility', 'visible');
            $("#success").css('visibility', 'hidden');
            $("#questionsubmit").css('visibility', 'visible');
  };
  
  // When you click on a poll the question box is displayed
  $scope.addanswers = function() {
        var id = $scope.idSelectedPoll;
        data = {id:id};
        JSON.stringify(data);
        $http({
        url: 'services/json_get_answers',
        method: "POST",
        datatype : "json",
        data: $.param(data),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
        $scope.answers = data;
        });
        
        }
        
  // Assign variables for the answer selected and make submit button now clickable
    $scope.setAnswer = function (answer,poll) {
        $scope.answer = answer;
        $scope.poll = poll;
        $scope.answerSelected = true;
        
    }
  
  // Submit the vote
    $scope.submitVote = function() {
        if($scope.answerSelected == false){
            alert("choose something!");
        }else{
            data = {answer:$scope.answer,poll:$scope.poll};
            JSON.stringify(data);
            $http({
            
            url: 'services/submit_vote',
            method: "POST",
            datatype : "json",
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (data) {
                $("#PollingBoothAnswersOuter").css('visibility', 'hidden');
                $("#success").css('visibility', 'visible');
                 $("#questionsubmit").css('visibility', 'hidden');
                   
            });
        }
    }
        
}
</script>